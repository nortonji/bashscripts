#!/bin/bash
###########################################################################
# Norton Ji - 2020-07-17
#
# Enviar email com informacoes da escala do sobreaviso
#
# Modificacoes
# 2020-07-29 - inclusao do calendario no teams, nao precisa mais cadastrar
#              o sobreaviso em $DIAS_DO_MES
# 2021-03-18 - Script agora faz curl no calendario ate $TENTATIVASMAX vezes 
#              antes de dar erro 
# 2021-09-28 - Troca do link do calendario e substituicao do Leandro pelo
#              Carlos Manoel
###########################################################################

DESTINATARIOS="email@here john@doe"
ADMIN="root@localhost"
CELULAR_SOBREAVISO="555-5555"
LINK_CALENDARIO="https://outlook.office365.com/reachcalendar.ics"
DONO_CALENDARIO="me@localhost"


HOJE=$(date +%Y%m%d) # dia de hoje YYYYMMDD

function enviar_email () {
	case $1 in
		sobreaviso)
		mail -r "root@cisne <epsism@localhost>" -b $ADMIN -s "Sobreaviso do suporte ao processamento sismico" "$DESTINATARIOS" <<- EOF
		Prezados,
		
		sobreaviso do suporte ao processamento sismico neste fim-de-semana: $ANALISTA
		Celular do sobreaviso: $CELULAR_SOBREAVISO
		EOF
		;;

		erro_formato)
		mail -r "root@cisne <epsism@localhost>" -s "ERRO! Calendario fora do padrao conhecido" "$ADMIN" <<- EOF
		Nao foi possivel enviar a escala de sobreaviso para o fim-de-semana pois o calendario esta
		fora do padrao conhecido. 

		Este script: $0
		EOF
		;;
	
		erro_calendario)
		mail -r "root@cisne <epsism@localhost>" -s "ERRO! Nao foi possivel obter calendario de sobreaviso" "$ADMIN" <<- EOF
		curl em
 
		$LINK_CALENDARIO

		 falhou! Nao foi possivel obter calendario do sobreaviso. Mensagem de erro:
		$CALENDARIO

		Tentativas de acesso ao calendario: $TENTATIVAS

		Este script: $0
		EOF
		;;
	
		erro_planejamento)
		mail -r "root@cisne <epsism@localhost>" -s "Nao existe planejamento de sobreaviso para o dia de Hoje ($HOJE)!" "$ADMIN" <<- EOF
		Nao existe planejamento de sobreaviso para o dia de hoje ($HOJE)! Atualizar o calendario em:
		$LINK_CALENDARIO
			
		e executar $0 de novo.
		EOF
		;;

		erro_planejamento_futuro)
		mail -r "root@cisne <epsism@localhost>" -s "Nao existe planejamento de sobreaviso para a data $HOJE_MAIS_7_DIAS" "$DONO_CALENDARIO" <<- EOF
		Nao existe planejamento de sobreaviso para a data ${HOJE_MAIS_7_DIAS} (YYYYMMDD). Eh necessario atualizar
		o calendario do sobreaviso em:
		$LINK_CALENDARIO 

		Este script: $0
		EOF
		;;
		
	esac
	
}

TENTATIVAS=0
TENTATIVASMAX=10
while [ $TENTATIVAS -lt $TENTATIVASMAX ]
do
	
	CALENDARIO=$(curl -m 5 -fsSL $LINK_CALENDARIO 2>&1)
	if [ "$?" -eq "0" ] ; then
		break
	fi
	TENTATIVAS=$(($TENTATIVAS+1))
	sleep 10
	[ $TENTATIVAS == $TENTATIVASMAX ] && enviar_email erro_calendario && exit 1 
	

done

CALENDARIO=$(echo "$CALENDARIO"|egrep 'SUMMARY|DTSTART;VALUE|DTEND;VALUE')
if [ "$?" -ne "0" ] ; then
	enviar_email erro_formato
	exit 1
	
fi

CALENDARIO=$(echo "$CALENDARIO"|tr -d '\r'|tr "\n" ";"|sed -e 's/SUMMARY:/\n/g;s/;VALUE=DATE:/;/g') #dos2unix e acertando campos
# EXEMPLO CALENDARIO
# Leandro;DTSTART;20200930;DTEND;20201007;
# Norton;DTSTART;20201007;DTEND;20201014;
# Leandro;DTSTART;20201014;DTEND;20201023;
# Norton;DTSTART;20201023;DTEND;20201125;
# F_rias Leandro;DTSTART;20201026;DTEND;20201125;
# Leandro;DTSTART;20201125;DTEND;20201202;
# Norton;DTSTART;20201202;DTEND;20201209;
# Leandro;DTSTART;20201209;DTEND;20201216;
# Norton;DTSTART;20201216;DTEND;20201223;
# Leandro;DTSTART;20201223;DTEND;20201230;
# Norton;DTSTART;20201230;DTEND;20210106;


# $1 no calendario soh pode ter as strings "carlos manoel" ou "norton" (case insensitive por causa do tolower)
ANALISTA=$(echo "$CALENDARIO" |awk -F\; -v var=$HOJE '{if  (($2 == "DTSTART")  &&  ($4 == "DTEND") && \
	                                                    ( var >= $3     )  &&  ( var < $5    ) && \
							    (tolower($1) == "carlos manoel" || tolower($1) == "norton")) \
							    {print $1; flag=1 }} END{if (!flag) print "indefinido"}')

if [ "$ANALISTA" != "indefinido" ] ; then
	#ssh bwss-tivit "mail -b $ADMIN -s \"Sobreaviso do suporte ao processamento sismico\" \"$DESTINATARIOS\" <<- EOF
	#Prezados,
	#
	#sobreaviso do suporte ao processamento sismico neste fim-de-semana: $ANALISTA
	#Celular do sobreaviso: $CELULAR_SOBREAVISO
	#EOF
	#"
	enviar_email sobreaviso
	
else
	#ssh bwss-tivit "mail -s \"Nao existe planejamento de sobreaviso para o dia de Hoje ($HOJE)!\" \"$ADMIN\" <<- EOF
	#Nao existe planejamento de sobreaviso para o dia de hoje ($HOJE)! Alterar o script e executar de novo.
	#EOF
	#"
	enviar_email erro_planejamento
	
fi

# script roda toda sexta. Se na proxima sexta nao tiver planejamento no calendario, avisar $DONO_CALENDARIO
HOJE_MAIS_7_DIAS=$(date -d "$HOJE + 7 days" +%Y%m%d)
ANALISTA_7_DIAS=$(echo "$CALENDARIO" |awk -F\; -v var=$HOJE_MAIS_7_DIAS '{if  (($2 == "DTSTART")  &&  ($4 == "DTEND") && \
	                                                                       ( var >= $3     )  &&  ( var < $5    ) && \
							                       (tolower($1) == "carlos manoel" || tolower($1) == "norton")) \
							                       {print $1; flag=1 }} END{if (!flag) print "indefinido"}')
if [ "$ANALISTA_7_DIAS" == "indefinido" ] ; then
	enviar_email erro_planejamento_futuro
fi       
